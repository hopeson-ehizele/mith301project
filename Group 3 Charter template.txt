Team charter / Contrato de equipo

Last updated / Última actualización: 2020-09-2009


Group name / Nombre del Equipo:
On The Way!


Roles / Roles
Technical leader / Líder técnicx : Hopeson Ehizele

Submitter / Encargadx de envíos: Claudia Aguilar / Federico Sardi


Team Members / Integrantes del equipo
* Hopeson Ehizele, (he/him/él), Maryland US, Technical Leader
* Claudia Aguilar, (She/her/ella), Maryland (US), Submitter
* Gabriela Striker, (she/her/ella), Buenos Aires (ARG)
* Federico Sardi, (he/him/él), Montevideo (Uruguay), Submitter


Goals / Objetivos
Our group hopes to successfully integrate and utilize digital humanities databases and software such as GitLab 
and Visual Studio Code in our Digital Humanities related coursework and project. As we progress in the semester, 
each of us will work to increase our familiarity with the digital humanities field by browsing databases and 
reading articles. Collectively and individually, we will work to become more comfortable with the use of software 
such as VS Code, GitLab, and Slack so that we can complete our tasks and assignments more efficiently. We will 
utilize Slack as our primary method of communication when we need to ask questions or update each other on our 
completion status on individual tasks. Using the knowledge and skills we acquire throughout the course of the 
semester, we will successfully complete each group assignment, which will culminate in the creation, publication, 
and maintenance of our website.

Nuestro grupo esperar integrar y utilizar con éxito bases de datos y software de Humanidades Digitales como GitLab 
y Visual Studio Code en nuestros cursos y proyectos relacionados con la materia. A medida que avancemos en el semestre, 
cada uno de nosotros trabajará para incrementar su familiaridad con el campo de las Humanidades Digitales mediante la 
exploración de bases de datos y la lectura de artículos. De forma colectiva e individual, trabajaremos para sentirnos 
más cómodos con el uso de software como VS Code, GitLab y Slack para que podamos completar nuestras tareas y asignaciones 
de manera más eficiente. Utilizaremos Slack como principal método de comunicación, cuando necesitemos hacer preguntas o 
mantenernos actualizados sobre el estado de las tareas individuales. Utilizando los conocimientos y las habilidades que 
adquiramos a lo largo del semestre, completaremos con éxito cada tarea grupal, que culminará con la creación, publicación 
y mantenimiento de nuestro sitio web.



Core Values / Fundamentos
As a group, we plan on achieving these goals by communicating with one another either via Slack or in the zoom class 
as well. Our group should be able to succeed as long as we maintain communication with one another whether having 
difficulties with using VSCode/GitLab or not being able to attend a meeting on a certain day. Our expectations are 
to be respectful to one another, be considerate of the multilingualism in the group, and to try and turn in assignments 
on time. In order to keep each other accountable, we will be aware of the deadlines and give friendly reminders if
one of our team members is starting to fall behind on tasks. We’re aware of our time zone differences. We’re also 
aware we are currently in a pandemic, meaning things might come up, but ultimately as a group, our main values are 
to be responsible, respect one another, and communicate any difficulties that may occur.

Como grupo, intentaremos cumplir con nuestros objetivos comunicándonos, tanto a través de Slack como por Zoom. Nuestro 
grupo llevará las tareas a buen término al mantener una fluida comunicación entre nosotros, ayudándonos si surgiera 
alguna dificultad en el uso de VSCode/GitLab o si alguno no pudiera asistir a las reuniones en una fecha determinada. 
Entendemos que el respeto es la base, por lo cual debemos ser especialmente considerados y pacientes, al estar trabajando 
en un grupo multilingüe. Parte de este respeto, no solamente entre nosotros, sino para con todos quienes participan del 
curso, implica entregar nuestras tareas a tiempo. Para mantenernos al día, estaremos atentos a las fechas de entrega y nos 
comunicaremos entre nosotros con el fin de que nadie quede atrás. También somos conscientes de estar atravesando tiempos 
difíciles debido a la pandemia, lo que quiere decir que podrían surgir algunos imprevistos, que deberemos afrontar como 
grupo, manteniendo nuestros valores de responsabilided, respeto y comunicación fluida, especialmente ante cualquier adversidad.


Team management / Gestión del equipo
The channel that we will use to communicate daily will be Slack. Through this tool we will share doubts, recommendations and 
comments. We will also use Slack to sketch an index with the topics to be addressed (as an agenda) in a virtual synchronous 
meeting in which we will participate through the Zoom platform. We will schedule this weekly meeting to discuss agenda items, 
divide specific tasks, and make decisions with the consensus of team members. In this session we can also express feelings and 
those individual or collective problems that distress us and resolve social or work conflicts through attentive listening and 
respectful dialogue. We will especially try to develop flexibility in conflict situations. If the complexity of the academic 
project or the severity of a conflict require it, we will undoubtedly add other virtual meetings to guarantee the continuity 
of the tasks, the fulfillment of the objectives as well as the emotional well-being of the team. Finally, as we pointed out 
in the “Roles” section, our team will have a technical leader and a submitter. According to the needs of the daily practice, 
we will establish new roles that will be informed in the successive updates of the contract. Those who occupy the two mandatory 
and optional roles may rotate their position with the prior agreement of all members in the weekly meeting.

El canal que utilizaremos para comunicarnos diariamente será Slack. Por medio de esta herramienta compartiremos dudas, 
recomendaciones y comentarios. También nos serviremos de Slack para esbozar un índice con los temas a abordar 
(a modo de orden del día) en un encuentro sincrónico virtual en el que participaremos a través de la plataforma Zoom. 
Programaremos esta reunión semanal para conversar sobre los temas de la agenda, dividir tareas específicas y tomar decisiones 
con el consenso de los miembros del equipo. En esta sesión también podremos expresar sentimientos y aquellas problemáticas 
individuales o colectivas que nos angustien y resolver conflictos sociales o laborales mediante la escucha atenta y el diálogo 
respetuoso. Procuraremos desarrollar  especialmente la flexibilidad ante situaciones conflictivas. Si la complejidad del proyecto 
académico o la gravedad de un conflicto lo requirieran, añadiremos sin duda otros encuentros virtuales para garantizar la continuidad 
de las tareas, el cumplimiento de los objetivos así como el bienestar emocional del equipo. Por último, como señalamos en el apartado 
“Roles”, nuestro equipo contará con un líder técnico y un encargado de envíos. Según las necesidades de la práctica diaria, 
estableceremos nuevos roles que serán informados en las sucesivas actualizaciones del contrato. Quienes ocupen los dos roles obligatorios 
y los opcionales podrán rotar de posición con previo acuerdo de todos los miembros en la reunión semanal.


Ethos /Ethos
* Open communication is key
* Empathy, patience and cooperation
* Try Again. Fail Again. Fail better!
* Do not lose your sense of humor. Smile!
