// Based on a script by Kathie Decora : katydecorah.com/code/lunr-and-jekyll/

// Create the lunr index for the search
var index = elasticlunr(function () {
  this.addField('title')
  this.addField('author')
  this.addField('layout')
  this.addField('content')
  this.setRef('id')
});

// Add to this index the proper metadata from the Jekyll content

index.addDoc({
  title: "404: Page not found",
  author: null,
  layout: "page",
  content: "Sorry, we've misplaced that URL or it's pointing to something that doesn't exist.\nHead back home to try finding it again.\n\n\n404: Página no encontrada\nDisculpa, hemos colocado mal esta URL o está apuntando a algo que no existe.\nVuelve al inicio para intentar encontrarla de nuevo.\n",
  id: 0
});
index.addDoc({
  title: "About Us!",
  author: null,
  layout: "page",
  content: "\n       Presentation\n       This website was created within the framework of the Digital Publishing with Minimal computing: humanities at a global scale seminar organized by the University of Maryland \n            (United States) and the Universidad del Salvador (Argentina) and directed by Dr. Raffaele Viglianti and Dr. Gimena del Rio Riande, with technical support from Mg. \n            Nidia Hernández and Lic. Romina de León, members of the laboratory of the Centro Argentino de Información Científica y Tecnológica that depends on the Consejo Nacional \n            de Investigaciones Científicas y Técnicas (CAICyT-CONICET) from Argentina.\n       \n       On the way! seeks to host the results of the activities carried out by four English and Spanish-speaking students during the course in order to disseminate their learning \n            process, their achievements and, furthermore, to promote collaborative work in the field of Digital Humanities. The international team intends to carry out a multilingual digital \n            edition of fragments of An Account of a Voyage up the River de la Plata (17th century literary work) by Acarette du Biscay from a critical and humanist perspective using Minimal \n            Computing techniques and open standards such as those of the Text Encoding Initiative (TEI-XML) for the processing and editing of texts.\n       \n       Team\n       \n        Claudia Aguilar (University of Maryland, United States)  will contribute to the team by helping with brainstorming ideas and with graphic \n            design knowledge which may help for the design of the website.\n       \n        Hopeson Ehizele (University of Maryland, United States) will utilize a background in programming and IT in order to contribute to the team.\n       \n        Federico Sardi (Universidad de la República, Montevideo, Uruguay) will provide help translating contents between Spanish and English.\n       \n        Gabriela Striker (Universidad de Buenos Aires, Argentina) will contribute with her philological, literary and historical knowledge and with her basic \n            experience in encoding texts in XML-TEI developed in the Argentine project Reis Trobadors (in progress) directed by Dr. Gimena del Rio.\n\nVersión en español",
  id: 1
});
index.addDoc({
  title: "Acerca de <i>On the way!</i>",
  author: null,
  layout: "page",
  content: "\n    Presentación\n      El presente sitio web fue creado en el marco del seminario Digital Publishing with Minimal computing: humanities at a global scale organizado por la University \n      of Maryland (Estados Unidos) y la Universidad del Salvador (Argentina) y dirigido por el Dr. Raffaele Viglianti y la Dra. Gimena del Rio Riande, con el soporte técnico \n      de la Mg. Nidia Hernández y la Lic. Romina de León, miembros del laboratorio del Centro Argentino de Información Científica y Tecnológica del Consejo Nacional de \n      Investigaciones Científicas y Técnicas (CAICyT-CONICET), Argentina.\n      \n      On the way! busca alojar los resultados de las actividades que realizan cuatro estudiantes de habla inglesa y española durante la cursada con el objeto \n        de difundir su proceso de aprendizaje, sus logros y, además, fomentar el trabajo colaborativo en el campo de las Humanidades Digitales. El equipo internacional \n        pretende realizar una edición digital multilingüe de fragmentos de Relación de un viaje al Río de la Plata de Acarette du Biscay (obra literaria del siglo XVII) desde una \n        perspectiva crítica y humanista empleando las técnicas de la computación mínima (Minimal Computing) y estándares abiertos como los de la Text Encoding Initiative \n        (TEI-XML) para el tratamiento y la edición de textos.\n      \n    Equipo\n        Claudia Aguilar (University of Maryland, Estados Unidos) aportará ideas y sus conocimientos en diseño gráfico, que pueden ser útiles en la creación del sitio web.\n\n        Hopeson Ehizele (University of Maryland, Estados Unidos) hará uso de sus conocimientos en IT y programación para contribuir con el equipo.\n\n        Federico Sardi (Universidad de la República, Montevideo, Uruguay) contribuirá en la traducción de los contenidos y asistiendo en la comunicación bilingüe Inglés-Español.\n\n        Gabriela Striker (Universidad de Buenos Aires, Argentina) contribuirá  con sus conocimientos filológicos, literarios e históricos y con su experiencia básica  en codificación de \n            textos en XML-TEI desarrollada en el proyecto argentino Reis Trobadors (en curso) dirigido por la Dra. Gimena del Rio.\n\nEnglish version",
  id: 2
});
index.addDoc({
  title: "Charter",
  author: null,
  layout: "page",
  content: "\n\n    Group name / Nombre del equipo\n       On the way! \n\n\n    Roles / Roles\n    Technical leader / Líder técnico: Hopeson Ehizele\n    Submitters / Encargados de envíos: Federico Sardi / Hopeson Ehizele\n    Technical assistant / Asistente técnico: Federico Sardi\n    Translator / Traductora: Claudia Aguilar\n    Coordinator / Coordinadora: Gabriela Striker\n\n    Team members / Integrantes del equipo\n        \n            Hopeson Ehizele (he/him/él), Maryland (US), Technical leader / Submitter \n            Claudia Aguilar (she/her/ella), Maryland (US), Translator\n            Gabriela Striker (she/her/ella), Buenos Aires (Argentina), Coordinator\n            Federico Sardi (he/him/él), Montevideo (Uruguay), Submitter / Technical assistant\n        \n\n    Goals / Objetivos\n        Our group hopes to successfully integrate and utilize digital humanities databases and software such as GitLab \n        and Visual Studio Code in our Digital Humanities related coursework and project. As we progress in the semester, \n        each of us will work to increase our familiarity with the digital humanities field by browsing databases and \n        reading articles. Collectively and individually, we will work to become more comfortable with the use of software \n        such as VS Code, GitLab, and Slack so that we can complete our tasks and assignments more efficiently. We will \n        utilize Slack as our primary method of communication when we need to ask questions or update each other on our \n        completion status on individual tasks. Using the knowledge and skills we acquire throughout the course of the \n        semester, we will successfully complete each group assignment, which will culminate in the creation, publication, \n        and maintenance of our website.\n\n        Nuestro grupo espera integrar y utilizar con éxito bases de datos y software de Humanidades Digitales como GitLab \n        y Visual Studio Code en nuestros cursos y proyectos relacionados con la materia. A medida que avancemos en el semestre, \n        cada uno de nosotros trabajará para incrementar su familiaridad con el campo de las Humanidades Digitales mediante la \n        exploración de bases de datos y la lectura de artículos. De forma colectiva e individual, trabajaremos para sentirnos \n        más cómodos con el uso de software como VS Code, GitLab y Slack para que podamos completar nuestras tareas y asignaciones \n        de manera más eficiente. Utilizaremos Slack como principal método de comunicación, cuando necesitemos hacer preguntas o \n        mantenernos actualizados sobre el estado de las tareas individuales. Utilizando los conocimientos y las habilidades que \n        adquiramos a lo largo del semestre, completaremos con éxito cada tarea grupal, que culminará con la creación, publicación \n        y mantenimiento de nuestro sitio web.\n\n\n\n    Values / Fundamentos\n        As a group, we plan on achieving these goals by communicating with one another either via Slack or in the zoom class \n        as well. Our group should be able to succeed as long as we maintain communication with one another whether having \n        difficulties with using VSCode/GitLab or not being able to attend a meeting on a certain day. Our expectations are \n        to be respectful to one another, be considerate of the multilingualism in the group, and to try and turn in assignments \n        on time. In order to keep each other accountable, we will be aware of the deadlines and give friendly reminders if\n        one of our team members is starting to fall behind on tasks. We’re aware of our time zone differences. We’re also \n        aware we are currently in a pandemic, meaning things might come up, but ultimately as a group, our main values are \n        to be responsible, respect one another, and communicate any difficulties that may occur.\n\n        Como grupo, intentaremos cumplir con nuestros objetivos comunicándonos, tanto a través de Slack como por Zoom. Nuestro \n        grupo llevará las tareas a buen término al mantener una fluida comunicación interna, ayudándonos si surgiera \n        alguna dificultad en el uso de VSCode/GitLab o si alguno no pudiera asistir a las reuniones en una fecha determinada. \n        Entendemos que el respeto es la base, por lo cual debemos ser especialmente considerados y pacientes, al estar trabajando \n        en un grupo multilingüe. Parte de este respeto, no solamente entre nosotros, sino para con todos quienes participan del \n        curso, implica entregar nuestras tareas a tiempo. Para mantenernos al día, estaremos atentos a las fechas de entrega y nos \n        comunicaremos entre nosotros con el fin de que nadie quede atrás. También somos conscientes de estar atravesando tiempos \n        difíciles debido a la pandemia, lo que quiere decir que podrían surgir algunos imprevistos, que deberemos afrontar como \n        grupo, manteniendo nuestros valores de responsabilided, respeto y comunicación fluida, especialmente ante cualquier adversidad.\n\n\n    Team management / Gestión del equipo\n        The channel that we will use to communicate daily will be Slack. Through this tool we will share doubts, recommendations and \n        comments. We will also use Slack to sketch an index with the topics to be addressed (as an agenda) in a virtual synchronous \n        meeting in which we will participate through the Zoom platform. We will schedule this weekly meeting to discuss agenda items, \n        divide specific tasks, and make decisions with the consensus of team members. In this session we can also express feelings and \n        those individual or collective problems that distress us and resolve social or work conflicts through attentive listening and \n        respectful dialogue. We will especially try to develop flexibility in conflict situations. If the complexity of the academic \n        project or the severity of a conflict require it, we will undoubtedly add other virtual meetings to guarantee the continuity \n        of the tasks, the fulfillment of the objectives as well as the emotional well-being of the team. Finally, as we pointed out \n        in the “Roles” section, our team will have a technical leader and a submitter. (According to the needs of the daily practice, \n        we will establish new roles that will be informed in the successive updates of the contract. Those who occupy the two mandatory \n        and optional roles may rotate their position with the prior agreement of all members in the weekly meeting).\n\n        El canal que utilizaremos para comunicarnos diariamente será Slack. Por medio de esta herramienta compartiremos dudas, \n        recomendaciones y comentarios. También nos serviremos de Slack para esbozar un índice con los temas a abordar \n        (a modo de orden del día) en un encuentro sincrónico virtual en el que participaremos a través de la plataforma Zoom. \n        Programaremos esta reunión semanal para conversar sobre los temas de la agenda, dividir tareas específicas y tomar decisiones \n        con el consenso de los miembros del equipo. En esta sesión también podremos expresar sentimientos y aquellas problemáticas \n        individuales o colectivas que nos angustien y resolver conflictos sociales o laborales mediante la escucha atenta y el diálogo \n        respetuoso. Procuraremos desarrollar  especialmente la flexibilidad ante situaciones conflictivas. Si la complejidad del proyecto \n        académico o la gravedad de un conflicto lo requirieran, añadiremos sin duda otros encuentros virtuales para garantizar la continuidad \n        de las tareas, el cumplimiento de los objetivos así como el bienestar emocional del equipo. Por último, como señalamos en el apartado \n        “Roles”, nuestro equipo contará con un líder técnico y un encargado de envíos. (Según las necesidades de la práctica diaria, \n        estableceremos nuevos roles que serán informados en las sucesivas actualizaciones del contrato. Quienes ocupen los dos roles obligatorios \n        y los opcionales podrán rotar de posición con previo acuerdo de todos los miembros en la reunión semanal).\n\n\n    Ethos\n    \n        Open communication is key / La comunicación fluida es clave\n        Empathy, patience and cooperation / Empatía, paciencia y cooperación\n        Try Again. Fail Again. Fail better! / Intenta de nuevo. Falla de nuevo. ¡Falla mejor!\n        Do not lose your sense of humor. Smile! / Nunca pierdas el sentido del humor. ¡Sonríe!\n    \n",
  id: 3
});
index.addDoc({
  title: "Criterios de edición",
  author: null,
  layout: "page",
  content: "\n   \n    Nuestra propuesta de edición digital de \"Description of Buenos Aires\"/\"Descripción de Buenos Aires\" se basa en los principios reunidos en TEI Simple Print Guidelines (2017).\n    Asimismo, ofrecemos versiones modernizadas de lectura, comprensibles para cualquier lector de lengua inglesa y española,  que incluyen la normalización gráfica y fonética de acuerdo con las convenciones lingüísticas actuales, el desarrollo de abreviaturas así como las enmiendas significativas y otras intervenciones editoriales.  Aquí detallamos los criterios específicos: \n        1. Regularización\n        Marcamos cada item normalizado con el elemento reg y señalamos la forma que se halla en la fuente con la etiqueta orig en los siguientes casos:\n        \n            1.1. Uso de mayúsculas\n            En ambas versiones empleamos mayúsculas para inicio de oración y nombres propios que marcamos con la etiqueta name y su atributo type. Además, la versión inglesa mantiene las mayúsculas en nacionalidades como English, Dutch, Portuguese.\n            1.2. En \"Description of Buenos Aires\":\n            \n                1.2.1. Grafema ſ/s\n                    Normalizamos las numerosas ſ  (s largas) que encontramos recurrentemente en el texto, por ejemplo, a principio de palabras (ſmall, ſurrounded, ſix, ſingle), en contextos intervocálicos de manera simple o doble (uſually, encloſure, neceſſity, impoſſible) y en sílabas finales (Engliſh, confeſs, paſs).\n                1.2.2. Grafema y/i\n                    Usamos i en lugar de y solo en diptongos que hallamos en términos españoles como Buenos Ayres.\n            \n            1.3. En \"Descripción de Buenos Aires\":\n            \n                1.3.1. Acentuación\n                Quitamos y añadimos tildes según las convenciones ortográficas españolas actuales.\n                1.3.2. Grafema f/h\n                Regularizamos formas arcaicas que comenzaban con f (fierro) y hoy se reconocen con h (hierro).\n                1.3.3. Grafema g/j\n                Adaptamos el uso de g/j (que antaño era indistinto y que encontramos, por ejemplo, en gefes) al uso ortográfico contemporáneo (jefes).\n                1.3.4. Grafema s/x\n                Incorporamos el uso de x en lugar de s en: estranjeros, estremos, estremadamente, escelentes, escepcion.\n            \n        \n        2. Desarrollo de abreviaturas\n        Indicamos las abreviaturas con el elemento abbr y luego las desarrollamos con el elemento expan para facilitar al lector moderno una clara interpretación del texto. Se pueden visualizar en color gris, por ejemplo,  “1 shilling 6 pence” es la forma desarrollada de la construcción abreviada “1 s. 6 d. English” y  “livers” representa la expansión de la abreviatura “l.” en la versión inglesa.\n        3. Separación de palabras\n        Separamos las palabras de acuerdo con la ortografía actual siempre que se conserve el valor semántico que ofrece la fuente. Por ejemplo, separamos apesar y hacepor en el primer párrafo de la versión española.\n        4. Enmiendas\n        Decidimos corregir solamente aquellos errores que podríamos atribuir al proceso de copia y que comprenden, a nuestro criterio, variantes significativas en relación con el uso semántico contemporáneo. Empleamos sic para marcar el error y corr para la forma enmendada. Por ejemplo, corregimos Riochuelo en ambas versiones y otras palabras como extreamly y frecuentemeate, buluarte, perque, peros, entre otras.\n        5. Otras intervenciones editoriales\n        \n            5.1. Errores por omisión\n                Empleamos la etiqueta supplied para añadir las letras omitidas, que pueden verse entre corchetes en los textos normalizados. En la versión española notamos, por ejemplo, varias ausencias de s a final de pronombres que son necesarias para la concordancia con el sustantivo en número plural: su[s]. \n            5.2. Extranjerismos\n                Mediante el elemento foreign indicamos los vocablos en lengua extranjera destacados en la fuente, como livers en el fragmento español, y conservamos su cursiva.\n        \n    \n\nEnglish version",
  id: 4
});
index.addDoc({
  title: "Editorial principles",
  author: null,
  layout: "page",
  content: "\n    \n    Our proposal for a digital edition of \"Description of Buenos Aires\"/\"Descripción de Buenos Aires\" is based on principles gathered in TEI Simple Print Guidelines (2017).\n    We also offer modernized reading versions, understandable to any English and Spanish language reader, which include graphic and phonetic normalization in accordance with current linguistic conventions, the development of abbreviations as well as significant amendments and other editorial interventions. Here we detaile the specific criteria: \n        1. Regularization\n        We marked up each normalized item with the reg element and the form found in the source with the orig tag in the following cases:\n        \n            1.1. Capitalization\n            In both versions we used capital letters for the beginning of the sentence and proper names that we marked up with the name tag and its type attribute. In addition, the English version keeps capital letters in nationalities such as English, Dutch, Portuguese.\n            1.2. In \"Description of Buenos Aires\":\n            \n                1.2.1. Grapheme ſ/s\n                    We normalized the numerous ſ  (long s) that we found recurrently, for example, at the beginning of words (ſmall, ſurrounded, ſix, ſingle), in intervocalic contexts in a simple or double way (uſually, encloſure, neceſſity, impoſſible) and in final syllables (Engliſh, confeſs, paſs).\n                1.2.2. Grapheme y/i\n                    We used i instead of y only in diphthongs that we found in Spanish terms like Buenos Ayres.\n            \n            1.3. In \"Descripción de Buenos Aires\":\n            \n                1.3.1. Accentuation\n                We removed and added accents according to current Spanish spelling conventions.\n                1.3.2. Grapheme f/h\n                We regularized archaic forms that began with f (fierro) and today are recognized with h (hierro).\n                1.3.3. Grapheme g/j\n                We adapted the use of g/j (which was once indistinct and that we found, for example, in gefes) to the contemporary orthographic use (jefes).\n                1.3.4. Grapheme s/x\n                We incorporated the use of x instead of s in: estranjeros, estremos, estremadamente, escelentes, escepcion.\n            \n        \n        2. Development of abbreviations\n    We indicated the abbreviations with the abbr element and then we developed them with the expan tag to facilitate the modern reader a clear interpretation of the text. They can be displayed in gray, for example, “1 shilling 6 pence” is the expanded form of the abbreviated construction “1 s. 6 d. English” and “livers” represents an expansion of the abbreviation “l.” in the English version.\n        3. Word division\n    We splitted the words according to the current spelling as long as the semantic value offered by the source is preserved. For example, we divided apesar and hacepor in the first paragraph of the Spanish version.\n        4. Amendments\n    We decided to correct only those mistakes that we can attribute to the copying process and that comprise, in our opinion, significant variations in relation to contemporary semantic use. We used sic to mark up the mistake and corr for the amended form. For example, we corrected Riochuelo in both versions and other words such as extreamly and frecuentemeate, buluarte, perque, peros, among others.\n        5. Other editorial interventions\n        \n            5.1. Mistakes by omission\n                We used supplied tag to add missing letters, which can be seen in square brackets in standard texts. In the Spanish version we noted, for example, several absences of s at the end of pronouns that are necessary for the agreement with the noun in plural number: su[s]. \n            5.2. Foreign words\n                By means of the foreign element we indicated the words in foreign language highlighted in the source, such as livers in the Spanish fragment, and we keep their italics.\n        \n\n\nVersión en español",
  id: 5
});
index.addDoc({
  title: "Hopeson Ehizele",
  author: null,
  layout: "page",
  content: "Welcome To My Bio Page! \n    \n        My name is Hopeson Ehizele and I am a junior computer science major at the University of Maryland College Park! \n    \n",
  id: 6
});
index.addDoc({
  title: "Claudia's bio",
  author: null,
  layout: "page",
  content: "\n    This is Claudia's bio\n        I study at the University of Maryland\n\n    I speak spanish and english. I was born in Lima-Peru and grew up in Maryland for most of my life\n    My major is Studio Art-Graphic Design\n\n[in spanish]\n\n \n    Este es el bio de Claudia\n        Yo estudio en la universidad de maryland\n\n    Yo hablo ingles y el español. Yo nací en Lima-Peru y crecí en el estado de Maryland por una gran parte de mi vida\n    Mi carrera de estudio es el Diseño Grafico\n\n\n\n",
  id: 7
});
index.addDoc({
  title: "Description of Buenos Aires",
  author: null,
  layout: "tei",
  content: "\n  Digital edition (English version)\n\n",
  id: 8
});
index.addDoc({
  title: "Descripción de Buenos Aires",
  author: null,
  layout: "tei",
  content: "\n  Edición digital (versión castellana)\n\n",
  id: 9
});
index.addDoc({
  title: "Federico Sardi",
  author: null,
  layout: "page",
  content: "\n     Principal bassoon of the Montevideo Philharmonic.\n     Born in Montevideo, Uruguay in 1990.\n    Studies\n    \n        2002-2008 - Degree in Music (Bassoon), Montevideo's Conservatory, Uruguay.\n        2016-2018 - Master in Music (Bassoon), Mannheim University, Germany.\n        2013 - Master in Cultural Management, Córdoba University, Argentina.\n    \n    Languages\n    \n        Spanish\n        English\n        Portuguese\n        Italian\n        German\n    \n\n\n",
  id: 10
});
index.addDoc({
  title: "Gabriela Striker",
  author: null,
  layout: "page",
  content: "\n    Gabriela Striker was born and lives in Buenos Aires, Argentina.\n    She has got a Literature degree by the Universidad de Buenos Aires (UBA).\n    Since 2015 she has been participating as an associate in the subject Literatura Española I (medieval) in this institution.\n    She is currently a doctoral fellow at the University of Buenos Aires. Her thesis is focused on medieval lyric, specially on alterity forms in Galician-Portuguese poems of the thirteenth century.\n    Also, she works as an editor of academic texts.\n\n    Languages\n    \n        Spanish\n        English\n        German\n        Portuguese\n    \n\n    Research projects\n    \n        (2018-2021) UBACyT “Nuevas tecnologías y saberes para el estudio de los textos hispanorromances más antiguos” directed by Leonardo Funes.\n        (2019-2021) FILOCyT “Reis Trobadors: reconstrucción filológico-musicológica de la lírica medieval románica con herramientas digitales” directed by Gimena del Rio Riande and codirected by Germán Rossi.\n        (2016-2018) “Digitalización de fondos bibliográficos del Seminario de Edición y Crítica Textual” directed by Leonardo Funes.\n    \n\n\n\n\n    Gabriela Striker\n\n    Gabriela Striker nació y vive en Buenos Aires, Argentina.\n    Es Licenciada y Profesora de Enseñanza Media y Superior en Letras por la Universidad de Buenos Aires (UBA).\n    Desde 2015 ha estado participando como adscripta en la cátedra de Literatura Española I (medieval) en esta institución.\n    Actualmente es becaria doctoral de la UBA. Su tesis se enfoca en la lírica medieval, especialmente en las formas discursivas de alteridad en cantigas gallego-portuguesas del siglo XIII.\n    Además, se desempeña como editora y correctora de textos académicos.\n\n    Idiomas\n    \n        Español \n        Inglés\n        Alemán\n        Portugués\n    \n    \n    Proyectos de investigación\n    \n        (2018-2021) UBACyT “Nuevas tecnologías y saberes para el estudio de los textos hispanorromances más antiguos” dirigido por Leonardo Funes.\n        (2019-2021) FILOCyT “Reis Trobadors: reconstrucción filológico-musicológica de la lírica medieval románica con herramientas digitales” dirigido por Gimena del Rio Riande y codirigido por Germán Rossi.\n        (2016-2018) “Digitalización de fondos bibliográficos del Seminario de Edición y Crítica Textual” dirigido por Leonardo Funes.\n    \n\n\n",
  id: 11
});
index.addDoc({
  title: null,
  author: null,
  layout: "default",
  content: "\n  Welcome to On the way!\n   On the way! seeks to host the results of the activities carried out by four English and Spanish-speaking students during the Digital Publishing with Minimal computing: humanities at a global scale seminar directed by Dr. Raffaele Viglianti and Dr. Gimena del Rio Riande. This international and multilingual team wants to promote collaborative work in the field of Digital Humanities, with the scope of learning and integrating digital humanities databases and software such as GitLab, Visual Studio Code and Jekyll. Its main goal is to develop and publish a multilingual digital edition of fragments of the (literary work) Account of a Voyage up the River de la Plata (17th century) by Acarette du Biscay from a critical and humanistic perspective using Minimal Computing techniques and open standards such as those of the Text Encoding Initiative (TEI-XML) for the processing and editing of texts.\n  English edition\n\n\n  ¡Bienvenidos a On the way!\n   On the way! busca alojar los resultados de las actividades que realizan cuatro estudiantes de habla inglesa y española durante el seminario Digital Publishing with Minimal computing: humanities at a global scale dirigido por el Dr. Raffaele Viglianti y la Dra. Gimena del Rio Riande. El equipo internacional y multilingüe pretende promover el trabajo colaborativo en el campo de las Humanidades Digitales, utilizando e integrando bases de datos y software de Humanidades Digitales como GitLab, Visual Studio Code y Jekyll. Su principal objetivo es desarrollar y publicar una edición digital multilingüe de fragmentos de la (obra literaria) Relación de un viaje al Río de la Plata de Acarette du Biscay (siglo XVII) desde una perspectiva crítica y humanista empleando las técnicas de la computación mínima (Minimal Computing) y estándares abiertos como los de la Text Encoding Initiative (TEI-XML) para el tratamiento y la edición de textos.\n  Edición en español\n",
  id: 12
});
index.addDoc({
  title: "Search",
  author: null,
  layout: "page",
  content: "\nThis is a simple search system. It will match most non-grammatical words.\n  Not all results will be highlighted on the page.\n\n\n\n\n\n\n\n",
  id: 13
});
index.addDoc({
  title: null,
  author: null,
  layout: null,
  content: "// Based on a script by Kathie Decora : katydecorah.com/code/lunr-and-jekyll/\n\n// Create the lunr index for the search\nvar index = elasticlunr(function () {\n  this.addField('title')\n  this.addField('author')\n  this.addField('layout')\n  this.addField('content')\n  this.setRef('id')\n});\n\n// Add to this index the proper metadata from the Jekyll content\n{% assign count = 0 %}{% for text in site.pages %}\nindex.addDoc({\n  title: {{text.title | jsonify}},\n  author: {{text.author | jsonify}},\n  layout: {{text.layout | jsonify}},\n  content: {{text.content | jsonify | strip_html}},\n  id: {{count}}\n});{% assign count = count | plus: 1 %}{% endfor %}\n\n// Builds reference data (maybe not necessary for us, to check)\nvar store = [{% for text in site.pages %}{\n  \"title\": {{text.title | jsonify}},\n  \"author\": {{text.author | jsonify}},\n  \"layout\": {{ text.layout | jsonify }},\n  \"link\": {{text.url | jsonify}},\n}\n{% unless forloop.last %},{% endunless %}{% endfor %}]\n\n// Query\nvar qd = {}; // Gets values from the URL\nlocation.search.substr(1).split(\"&\").forEach(function(item) {\n    var s = item.split(\"=\"),\n        k = s[0],\n        v = s[1] && decodeURIComponent(s[1]);\n    (k in qd) ? qd[k].push(v) : qd[k] = [v]\n});\n\nfunction doSearch() {\n  var resultdiv = document.querySelector('#results');\n  var query = document.querySelector('input#search').value;\n\n  // The search is then launched on the index built with Lunr\n  var result = index.search(query);\n  resultdiv.innerHTML = \"\";\n  if (result.length == 0) {    \n    resultdiv.append(document.createElement('p').innerHTML = 'No results found.');\n  } else if (result.length == 1) {\n    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' result');\n  } else {\n    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' results');\n  }\n  // Loop through, match, and add results\n  for (var item in result) {\n    var ref = result[item].ref;\n    res = document.createElement('div')\n    res.classList.add(\"result\")\n    link = document.createElement('a')\n    link.setAttribute('href', '{{site.baseurl}}'+store[ref].link+'?q='+query)\n    link.innerHTML = store[ref].title || 'Untitled page'\n    p = document.createElement('p')\n    p.appendChild(link)\n    res.appendChild(p)\n    resultdiv.appendChild(res)\n  }\n}\n\nvar callback = function(){\n  searchInput = document.querySelector('input#search') \n  if (qd.q) {\n    searchInput.value = qd.q[0];\n    doSearch();\n  }\n  searchInput.addEventListener('keyup', doSearch);\n};\n\nif (\n    document.readyState === \"complete\" ||\n    (document.readyState !== \"loading\" && !document.documentElement.doScroll)\n) {\n  callback();\n} else {\n  document.addEventListener(\"DOMContentLoaded\", callback);\n}\n",
  id: 14
});
index.addDoc({
  title: null,
  author: null,
  layout: null,
  content: "/*\n  Common Variables\n\n  Feel free to change!\n*/\n\n/* Fonts */\n$main-font: \"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;\n$heading-font: sans-serif;\n$regular-font-size: 1.25em; /* 20px / 16px = 1.25em; support text resizing in all browsers */\n\n\n/*\n  Color\n\n  Make sure to leave color-scheme in `_config.yml` file empty for granular control\n*/\n\n$text-color: #1a1a1a;\n$heading-color: #057f8f;\n$link-color: #7a0000;\n\n@import \"ed\";\n@import \"syntax\";\n@import \"CETEIcean.css\";\n\n.home {text-align: justify;}\n\ntei-choice tei-abbr + tei-expan:before,\ntei-choice tei-expan + tei-abbr:before,\ntei-choice tei-sic + tei-corr:before,\ntei-choice tei-corr + tei-sic:before,\ntei-choice tei-orig + tei-reg:before,\ntei-choice tei-reg + tei-orig:before {\n  content: \"\";\n}\ntei-choice tei-abbr + tei-expan:after,\ntei-choice tei-expan + tei-abbr:after,\ntei-choice tei-sic + tei-corr:after,\ntei-choice tei-corr + tei-sic:after,\ntei-choice tei-orig + tei-reg:after,\ntei-choice tei-reg + tei-orig:after {\n  content: \"\";\n}\n\ntei-supplied:before {\n  content: \"[\";\n}\n\ntei-supplied:after {\n  content: \"]\";\n}\n\ntei-p {\n  text-indent: 20px;\n}\n\np {\n  text-align: justify\n}\n\ntei-abbr {\n  display: none;\n}\n\ntei-expan {\n  color: grey\n}\n\ntei-expan {\n  color: rgb(114, 112, 112); font-style: italic bold;\n}\n\ntei-foreign {\n  font-style: italic;\n}\n\ntei-sic {\n  display: none;\n}\n\ntei-orig {\n  display: none;\n}\n\ntei-div > tei-head {\n  color: #057f8f;\n  text-indent: 0em;\n}\n\ntei-name {color: rgb(248, 121, 74)}\n\ntei-fw {\n  display: none;\n}\n\nb {color: purple}",
  id: 15
});

// Builds reference data (maybe not necessary for us, to check)
var store = [{
  "title": "404: Page not found",
  "author": null,
  "layout": "page",
  "link": "/404.html",
}
,{
  "title": "About Us!",
  "author": null,
  "layout": "page",
  "link": "/_pages/About/",
}
,{
  "title": "Acerca de <i>On the way!</i>",
  "author": null,
  "layout": "page",
  "link": "/_pages/Acerca/",
}
,{
  "title": "Charter",
  "author": null,
  "layout": "page",
  "link": "/_pages/Charter/",
}
,{
  "title": "Criterios de edición",
  "author": null,
  "layout": "page",
  "link": "/_pages/Criteriosdeedicion/",
}
,{
  "title": "Editorial principles",
  "author": null,
  "layout": "page",
  "link": "/_pages/Editorialprinciples/",
}
,{
  "title": "Hopeson Ehizele",
  "author": null,
  "layout": "page",
  "link": "/_pages/Hopeson/",
}
,{
  "title": "Claudia's bio",
  "author": null,
  "layout": "page",
  "link": "/_pages/claudia/",
}
,{
  "title": "Description of Buenos Aires",
  "author": null,
  "layout": "tei",
  "link": "/_pages/example1_Description/",
}
,{
  "title": "Descripción de Buenos Aires",
  "author": null,
  "layout": "tei",
  "link": "/_pages/example2_Descripcion/",
}
,{
  "title": "Federico Sardi",
  "author": null,
  "layout": "page",
  "link": "/_pages/federicosardi/",
}
,{
  "title": "Gabriela Striker",
  "author": null,
  "layout": "page",
  "link": "/_pages/gabrielastriker/",
}
,{
  "title": null,
  "author": null,
  "layout": "default",
  "link": "/",
}
,{
  "title": "Search",
  "author": null,
  "layout": "page",
  "link": "/_pages/search/",
}
,{
  "title": null,
  "author": null,
  "layout": null,
  "link": "/assets/js/search.js",
}
,{
  "title": null,
  "author": null,
  "layout": null,
  "link": "/assets/css/style.css",
}
]

// Query
var qd = {}; // Gets values from the URL
location.search.substr(1).split("&").forEach(function(item) {
    var s = item.split("="),
        k = s[0],
        v = s[1] && decodeURIComponent(s[1]);
    (k in qd) ? qd[k].push(v) : qd[k] = [v]
});

function doSearch() {
  var resultdiv = document.querySelector('#results');
  var query = document.querySelector('input#search').value;

  // The search is then launched on the index built with Lunr
  var result = index.search(query);
  resultdiv.innerHTML = "";
  if (result.length == 0) {    
    resultdiv.append(document.createElement('p').innerHTML = 'No results found.');
  } else if (result.length == 1) {
    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' result');
  } else {
    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' results');
  }
  // Loop through, match, and add results
  for (var item in result) {
    var ref = result[item].ref;
    res = document.createElement('div')
    res.classList.add("result")
    link = document.createElement('a')
    link.setAttribute('href', '/mith301project'+store[ref].link+'?q='+query)
    link.innerHTML = store[ref].title || 'Untitled page'
    p = document.createElement('p')
    p.appendChild(link)
    res.appendChild(p)
    resultdiv.appendChild(res)
  }
}

var callback = function(){
  searchInput = document.querySelector('input#search') 
  if (qd.q) {
    searchInput.value = qd.q[0];
    doSearch();
  }
  searchInput.addEventListener('keyup', doSearch);
};

if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
  callback();
} else {
  document.addEventListener("DOMContentLoaded", callback);
}
